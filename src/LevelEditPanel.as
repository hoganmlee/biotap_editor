package
{
	import com.bit101.components.HUISlider;
	import com.bit101.components.RangeSlider;
	import com.bit101.components.VRangeSlider;
	import com.bit101.components.VUISlider;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;

	public class LevelEditPanel extends LevelPanel
	{
		
		public var playStage:Signal = new Signal();
		public var saveStage:Signal = new Signal();
		public var loadStage:Signal = new Signal();
		public var closePanel:Signal = new Signal();
		
		public var stageSelected:Signal = new Signal(int);
		
		public var addB:Signal = new Signal();
		public var removeB:Signal = new Signal();
		public var addC:Signal = new Signal();
		public var removeC:Signal = new Signal();
		public var addS:Signal = new Signal();
		public var removeS:Signal = new Signal();
		public var addK:Signal = new Signal();
		public var removeK:Signal = new Signal();
		
		
		private var _numCompounds:int;
		
		
		public var bacteriaForce:HUISlider;
		public var bacteriaSpeed:HUISlider;
		public var heatStart:VUISlider;
		public var heatMaxMin:VRangeSlider;
		public var heatCoolRate:VUISlider;
		public var heatHeatRate:VUISlider;
		public var compounds:VUISlider;
		public var btuMultiplier:VUISlider;
		public var reaction:VUISlider;
		
		
		
		
		public function LevelEditPanel()
		{
			super();
						
			this.closeBTN.buttonMode = true;
			this.closeBTN.addEventListener(MouseEvent.CLICK, panelClose);
			
			this.addBacteria.buttonMode = true;
			this.addBacteria.addEventListener(MouseEvent.CLICK, bacteriaAdd);
			this.removeBacteria.buttonMode = true;
			this.removeBacteria.addEventListener(MouseEvent.CLICK, bacteriaRemove);
			
			this.addCompound.buttonMode = true;
			this.addCompound.addEventListener(MouseEvent.CLICK, compoundAdd);
			this.removeCompound.buttonMode = true;
			this.removeCompound.addEventListener(MouseEvent.CLICK, compoundRemove);
			this.addSticky.buttonMode = true;
			this.addSticky.addEventListener(MouseEvent.CLICK, stickyAdd);
			this.removeSticky.buttonMode = true;
			this.removeSticky.addEventListener(MouseEvent.CLICK, stickyRemove);
			this.addKiller.buttonMode = true;
			this.addKiller.addEventListener(MouseEvent.CLICK, killerAdd);
			this.removeKiller.buttonMode = true;
			this.removeKiller.addEventListener(MouseEvent.CLICK, killerRemove);
			
			this.saveLevel.buttonMode = true;
			this.saveLevel.addEventListener(MouseEvent.CLICK, saveDispatch);
			
			this.loadLevel.buttonMode = true;
			this.loadLevel.addEventListener(MouseEvent.CLICK, loadDispatch);
			
			this.playLevel.buttonMode = true;
			this.playLevel.addEventListener(MouseEvent.CLICK, playDispatch);
			
			
			this.chooseStage.stage1.buttonMode = true;
			this.chooseStage.stage1.addEventListener(MouseEvent.CLICK, stageChoose);
			this.chooseStage.stage2.buttonMode = true;
			this.chooseStage.stage2.addEventListener(MouseEvent.CLICK, stageChoose);
			this.chooseStage.stage3.buttonMode = true;
			this.chooseStage.stage3.addEventListener(MouseEvent.CLICK, stageChoose);
			this.chooseStage.stage4.buttonMode = true;
			this.chooseStage.stage4.addEventListener(MouseEvent.CLICK, stageChoose);
						
			
			bacteriaForce = new HUISlider(this, 30, 400, "Bacteria Tap Force");
			bacteriaForce.width = 400;
			bacteriaForce.minimum = 1.0;
			bacteriaForce.maximum = 6.0;
			bacteriaForce.value = 3;
			
			bacteriaSpeed = new HUISlider(this, 30, 430, "Bacteria Speed");
			bacteriaSpeed.width = 400;
			bacteriaSpeed.minimum = 1.0;
			bacteriaSpeed.maximum = 6.0;
			bacteriaSpeed.value = 3;
			
			heatStart = new VUISlider(this, 30, 455, "Heat Max / Min / Start");
			heatStart.height = 200;
			heatStart.value = 60;
			
			heatMaxMin = new VRangeSlider(this, 40, 475);
			heatMaxMin.height = 160;
			heatMaxMin.highValue = 90;
			heatMaxMin.lowValue = 40
			
			heatCoolRate = new VUISlider(this, 150, 455, "Cool Rate");
			heatCoolRate.maximum = 3;
			heatCoolRate.minimum = 0.2;
			heatCoolRate.value = 1;
			heatCoolRate.height = 200;
			
			heatHeatRate = new VUISlider(this, 200, 455, "Heat Rate");
			heatHeatRate.maximum = 40;
			heatHeatRate.minimum = 5;
			heatHeatRate.value = 20;
			heatHeatRate.height = 200;
			
			compounds = new VUISlider(this, 260, 455, "Compounds", calcBTU);
			compounds.maximum = 100;
			compounds.minimum = 20;
			compounds.height = 200;
			compounds.value = 100;
			
			reaction = new VUISlider(this, 330, 455, "Reaction");
			reaction.maximum = 2000;
			reaction.minimum = 100;
			reaction.height = 200;
			reaction.value = 100;
			
			btuMultiplier = new VUISlider(this, 400, 455, "x BTU", calcBTU);
			btuMultiplier.maximum = 2000;
			btuMultiplier.minimum = 100;
			btuMultiplier.height = 200;
			btuMultiplier.value = 1000;
			
			calcBTU();
			
		}
		
		public function set numCompounds(value:int):void
		{
			_numCompounds = value;
			calcBTU();
		}

		protected function bacteriaRemove(event:MouseEvent):void
		{
			removeB.dispatch();
		}
		
		protected function bacteriaAdd(event:MouseEvent):void
		{
			addB.dispatch();
		}
		
		public function get timeLimit():int
		{
			return int(this.timeTXT.text);
		}
		
		public function set timeLimit(value:int):void
		{
			this.timeTXT.text = value.toString();
		}
		
		private function stageChoose(e:MouseEvent):void
		{
			switch(e.currentTarget){
				
				case this.chooseStage.stage1:
					changeStage(0);
					break;
				case this.chooseStage.stage2:
					changeStage(1);	
					break;
				case this.chooseStage.stage3:
					changeStage(2);
					break;
				case this.chooseStage.stage4:
					changeStage(3);
					break;
				
			}
		}
		
		public function changeStage(value:int):void
		{
			switch(value){
				
				case 0:
					this.chooseStage.highlightMC.x = this.chooseStage.stage1.x;
					stageSelected.dispatch(0);
					break;
				
				case 1:
					this.chooseStage.highlightMC.x = this.chooseStage.stage2.x;
					stageSelected.dispatch(1);	
					break;
				
				case 2:
					this.chooseStage.highlightMC.x = this.chooseStage.stage3.x;
					stageSelected.dispatch(2);
					break;
				
				case 3:
					this.chooseStage.highlightMC.x = this.chooseStage.stage4.x;
					stageSelected.dispatch(3);
					break;
								
			}
			
		}
		
		protected function playDispatch(event:MouseEvent):void
		{
			playStage.dispatch();
		}
		
		protected function loadDispatch(event:MouseEvent):void
		{
			loadStage.dispatch();
		}
		
		protected function saveDispatch(event:MouseEvent):void
		{
			saveStage.dispatch();
		}
		
		protected function killerRemove(event:MouseEvent):void
		{
			removeK.dispatch();
		}
		
		protected function killerAdd(event:MouseEvent):void
		{
			addK.dispatch();
		}
		
		protected function stickyRemove(event:MouseEvent):void
		{
			removeS.dispatch();
		}
		
		protected function stickyAdd(event:MouseEvent):void
		{
			addS.dispatch();
		}
		
		
		private function calcBTU(e:Event=null):void
		{
			btuTXT.text = String(int(_numCompounds) * int(compounds.value) * int(btuMultiplier.value));
		}
		
		protected function compoundRemove(event:MouseEvent):void
		{
			if(_numCompounds > 0) _numCompounds--;
			calcBTU();
			removeC.dispatch();
		}
		
		protected function compoundAdd(event:MouseEvent):void
		{
			_numCompounds++;
			calcBTU();
			addC.dispatch();
		}
		
		protected function panelClose(event:MouseEvent):void
		{
			closePanel.dispatch();
		}		
		
		
		
	}
}