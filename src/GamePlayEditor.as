package
{	
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.*;
	import com.sajakfarki.biopets.game.base.*;
	import com.sajakfarki.biopets.game.levels.*;
	import com.sajakfarki.biopets.game.scenes.*;
	import com.sajakfarki.biopets.systems.*;
	import com.sajakfarki.biopets.utils.*;
	
	import de.nulldesign.nd2d.display.World2D;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.Dictionary;
	
	import net.hires.debug.Stats;
	
	import org.osflash.signals.Signal;
			
	public class GamePlayEditor extends HoganWorld
	{

		private static const WIDTH:Number = 1024;
		private static const HEIGHT:Number = 768;
//		private static const WIDTH:Number = 960;
//		private static const HEIGHT:Number = 600;
		
		
		/** Music */
		[Embed(source = "../sounds/bg.mp3")]
		private var _bgMP3:Class;
		private var _bgMusic:Sound = new _bgMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/InfoMusic.mp3")]
		private var _infoMP3:Class;
		private var _infoMusic:Sound = new _infoMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/LevelComplete.mp3")]
		private var _completeMP3:Class;
		private var _completeMusic:Sound = new _completeMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/LevelFail.mp3")]
		private var _failMP3:Class;
		private var _failMusic:Sound = new _failMP3() as Sound;
		
		private var _channel:SoundChannel;
		
		
		
		public var levelFinished:Signal = new Signal();		
		
		
		
		private var _gameScene : GameScene;
		
		
		//Level Interface 
		private var _level : ILevel;
		
		public static var stats:Stats = new Stats();
		
		
		private var _stageVars:StageState;
		
		public function GamePlayEditor()
		{
			// touch
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			enableErrorChecking = false;
			
			antialiasing = 1;
			
			super(Context3DRenderMode.AUTO, 60);
			
		}
		
		/************************************************
		 * 
		 *		Override Added To Stage , Main Loop
		 * 
		 ************************************************/
		
		override protected function mainLoop(e:Event):void
		{
			super.mainLoop(e);
			stats.update(statsObject.totalDrawCalls, statsObject.totalTris);	
		}
		
		override protected function context3DCreated(e:Event):void
		{
			super.context3DCreated(e);
			
			if (context3D)
			{
				stats.driverInfo = context3D.driverInfo;
			}
			
		}
		
		override protected function addedToStage(event:Event):void
		{
			super.addedToStage(event);
			
			stats.y = HEIGHT - 100;
			addChild(stats);
		}
	
		/**********************************************************
		 * 
		 * 	Start Level
		 * 
		 **********************************************************/
		public function startLevel(currentPhase:int, numCompounds:int, gemsPerCompound:int, btuMultiplier:int, stageVars:StageState):void
		{	
			//set level
			GameState.stage1Collected = 0.1;
			GameState.stage2Collected = 0.1;
			GameState.stage3Collected = 0.1;
			GameState.stageGems = 0;
			
			GameState.CURRENT_PHASE = currentPhase;
			GameState.gemsCollected = gemsPerCompound * numCompounds;
			GameState.totalGemsPossible = gemsPerCompound * numCompounds;
			GameState.btuMultiplier = btuMultiplier;
						
			_stageVars = stageVars;
			
			loadStage();
			
		}
		
		private function quitGame():void
		{
			_level.pause();			
			_level.remove();	
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(quitGameDone);
		}
		
		private function quitGameDone():void
		{
			trace(">>>>>>> quitGameDone");
			_gameScene.doneHide.removeAll();
			
			disposeGame();
			
//			TweenMax.delayedCall(1, showSelectScene);
			TweenMax.delayedCall(1, levelFinished.dispatch);
		}
		
		private function retryStage():void
		{
			GameState.gemsCollected = GameState.gemsRetry;
			
			_level.pause();
			_level.remove();
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(retryDone);
			
		}
		
		private function retryDone():void
		{
			trace(">>>>>>> quitGameDone");
			
			//_gameScene.killTweens();
			disposeGame();
			
			TweenMax.delayedCall(1 , loadStage);
		}
		
		/**********************************************************
		 * 
		 *		Load Stage
		 * 
		 **********************************************************/
		private function loadStage():void
		{
			_gameScene = new GameScene(WIDTH, HEIGHT);
			_gameScene.paused.add(pauseGame);
			_gameScene.help.add(helpGame);
			_gameScene.start.add(startGame);
			_gameScene.retry.add(retryStage);
			_gameScene.quit.add(quitGame);
			setActiveScene(_gameScene);
			
			
			trace(">>>>> Load Stage");
			
			
			/** Reset gems */
			GameState.stageGems = 0;
			GameState.gemsRetry = GameState.gemsCollected;
			
			
			start();
			
			
			/**  Set the Stage */
			_level = new Level_Default(_gameScene, _gameScene.gameBatches, WIDTH, HEIGHT, GameState.gemsCollected, _stageVars);

			/**  Add Listeners */
			_level.initialized.add(showLevel);
			_level.stageFinished.add(finishedStage);
			_level.timesup.add(endLevel);
			
		}
		
		private function helpGame():void
		{
			_level.hidePanel();
		}
		
		private function showLevel():void
		{
			if(_channel){
				_channel.stop();
			}
			_channel = _infoMusic.play(0, 99);
			
			_level.initialized.remove(showLevel);
			
			//_level done initializing
			TweenMax.delayedCall(0.5, _gameScene.show);
		}
		
		private function endLevel():void
		{
			trace("_level.bacteriaCount = " + _level.bacteriaCount);
			
			trace("_level.levelTime = " + _level.levelTime);
			
			if(_level.bacteriaCount <= 0){
				
				finishedStage(false, 1);
				
			}else{
								
				if(GameState.stageGems > GameState.totalGemsPossible/2){
					
					finishedStage(true);
					
				}else{
					
					finishedStage(false, 2);
					
				}
				
			}
			
		}
		
		private function startGame():void
		{
			//Start Game Button pressed
			if(_channel){
				_channel.stop();
			}
			_channel = _bgMusic.play(0, 99);
			
//			_level.resume();
			TweenMax.delayedCall(0.2, resumeLevel);
		}
		
		/************************************************
		 * 
		 *	  Game Pause
		 * 
		 ************************************************/
		private function pauseGame(value:Boolean):void
		{
			
			if(value){
				
				pauseLevel();
								
			}else{
				
				resumeLevel();
				
			}
		
		}
		
		/**********************************************************
		 * 
		 * 	Pause/Resume Level
		 * 
		 **********************************************************/
		private function resumeLevel():void
		{
			TweenMax.killDelayedCallsTo(_level.resume);
			
			_level.hidePanel();
			TweenMax.delayedCall(1.5, _level.resume);
		}
		
		private function pauseLevel():void
		{
			TweenMax.killDelayedCallsTo(_level.resume);
			_level.showPanel();
			_level.pause();
			
		}
		
		/**********************************************************
		 * 
		 * 	Level / Stage Finished
		 * 
		 **********************************************************/
		private function finishedStage(success:Boolean, type:int=-1):void
		{
			_level.pauseTime();				
			_level.remove();					//tween out progress bar
			_gameScene.hidePause();
			
			/**  Remove Listeners */
			_level.stageFinished.remove(finishedStage);
			
			/** Advance Stage Position if Success */
			if(success){
				
				
				_channel.stop();
				_channel = _completeMusic.play();
				
				//GameState.CURRENT_PHASE++;
				//GameState.CURRENT_PHASE++;
				GameState.gemsRetry = GameState.gemsCollected;
				GameState.gemsCollected = GameState.stageGems;
				
/*				if(GameState.CURRENT_PHASE == 3){
					
					trace("Level Complete!");
					
					_level.levelFinished.add(finish);
					_level.retry.add(retryStage);
					_level.unlock.add(unlockAchieve);
					_level.showLevelComplete(1.2, 1);
					_level.showTitle(1.1, 1.1);
					
					
				}else{
					
					switch(GameState.CURRENT_PHASE){
						
						case 0:
							GameState.stage1Collected = GameState.gemsCollected;
							break;
						case 1:
							GameState.stage2Collected = GameState.gemsCollected;
							break;
						case 2:
							GameState.stage3Collected = GameState.gemsCollected;
							break;
					}*/
					
					
					/** Show Stage Complete */
					_level.nextStage.removeAll();
					_level.nextStage.add(nextStage);
					_level.showStageComplete(0.7, 1);
					
//				}
				
			}else{
				
				trace("fail type = " + type);
				
				_channel.stop();
				_channel = _failMusic.play();
				
				_level.levelFinished.add(finish);
				_level.retry.add(retryStage);
				_level.showStageFail(type);
				
				//TODO: FAIL! pass type of fail

			}
			
		}

		
		/**********************************************************
		 * 
		 * 	Stage Complete
		 * 
		 **********************************************************/
		private function nextStage():void
		{
			trace("Next Stage - >");
			_level.pause();
			_level.remove()
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(nextStageDone);
		}
		
		private function nextStageDone():void
		{
			trace(">>>>>>> nextStageDone");
			
			_gameScene.doneHide.removeAll();
			//sleep();
//			GameState.CURRENT_PHASE++;
			
			//_gameScene.killTweens();
			disposeGame();
			
//			TweenMax.delayedCall(1 , loadStage);
			
			
			
			//TODO: back to editor
			
			
			levelFinished.dispatch();
			
			
			
			
		}
				
		/**********************************************************
		 * 
		 * 	Level Complete
		 * 
		 **********************************************************/
		private function finish():void
		{
			
			GameState.globalBTUs += GameState.btuMultiplier * GameState.gemsCollected;
			
			_level.pause();
			_level.remove();
			
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(finishDone);
			
			_gameScene.hide();
			
		}
		
		private function finishDone():void
		{
			trace(">>>>>>> finishDone");
			
			_gameScene.doneHide.removeAll();
			
			disposeGame();

			//TweenMax.delayedCall(1 , levelFinished.dispatch);
			levelFinished.dispatch();
			
		}
		
		/**********************************************************
		 * 
		 *    Clean up _gameScene / _level , keep the contextView persistant  
		 * 
		 **********************************************************/
		private function disposeGame():void
		{
			if(_channel){
				_channel.stop();
			}
			
			if(_level){
				_level.disposeLevel();
				_level = null;
			}
			
			if(_gameScene){
				_gameScene.dispose();
				_gameScene = null;
			}
			
		}
		
	}
}
