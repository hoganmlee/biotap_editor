package
{
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.StageState;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.ByteArray;
	
	import flashx.textLayout.elements.BreakElement;
	
	[SWF (backgroundColor="#000000", width="1024", height="768")]
	public class biotap_level_editor extends Sprite
	{
		//Game class
		private var _gp:GamePlayEditor;
		
		private var stageState:StageState;
			
		private var _isOpen:Boolean = true;
		
		private var _levelEditPanel:LevelEditPanel
		private var _levelHolder:LevelBackground
		
		private var _stageSelect:int = 0;
				
		private var _xml:XML;
		
		private var _fileReference:FileReference
		
		private var _fileName:String = "level#_stage#.biotap";
		
		public function biotap_level_editor()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addedToStage(event:Event):void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			_levelHolder = new LevelBackground(1024, 768);
			addChild(_levelHolder);
			
			
			_levelEditPanel = new LevelEditPanel();
			_levelEditPanel.playStage.add(levelPlay);
			_levelEditPanel.loadStage.add(levelLoad);
			_levelEditPanel.saveStage.add(levelSave);
			_levelEditPanel.closePanel.add(panelClose);
			_levelEditPanel.addB.add(addBacteria);
			_levelEditPanel.removeB.add(removeBacteria);
			_levelEditPanel.addC.add(addCompound);
			_levelEditPanel.addK.add(addKiller);
			_levelEditPanel.addS.add(addSticky);
			_levelEditPanel.removeC.add(removeCompound);
			_levelEditPanel.removeK.add(removeKiller);
			_levelEditPanel.removeS.add(removeSticky);
			_levelEditPanel.stageSelected.add(selectStage);
			
			
			
			
			addChild(_levelEditPanel);
			
			
			
//			trace(_levelEditPanel.bacteriaForce.value);
			
			
			
		}
		
		private function removeBacteria():void
		{
			_levelHolder.removeBacteria();
		}
		
		private function addBacteria():void
		{
			_levelHolder.addBacteria();
		}
		
		private function selectStage(value:int):void
		{
//			trace("stageSelected.dispatch("+ value +")");
			_stageSelect = value;
		}
		
		private function removeSticky():void
		{
			_levelHolder.removeSticky();
		}
		
		private function removeKiller():void
		{
			_levelHolder.removeKiller();
		}
		
		private function removeCompound():void
		{
			_levelHolder.removeCompound();
		}
		
		private function addSticky():void
		{
			_levelHolder.addSticky()
		}
		
		private function addKiller():void
		{
			_levelHolder.addKiller();
		}
		
		private function addCompound():void
		{
			_levelHolder.addCompound();
		}
		
		private function panelClose():void
		{
			if(_isOpen){
				_isOpen = false;
				TweenLite.to(_levelEditPanel, 0.6, {x:-490, ease:Quint.easeInOut});
			}else{
				_isOpen = true;
				TweenLite.to(_levelEditPanel, 0.6, {x:0, ease:Quint.easeInOut});
			}
			
		}
		
		private function levelSave():void
		{
			var s:String = "";
			
			s += "GameState.gemsPerCompound = " + int(_levelEditPanel.compounds.value) + "\n";
			s += "GameState.totalGemsPossible = " + _levelHolder.compounds.length * int(_levelEditPanel.compounds.value) + "\n";
			s += "GameState.btuMultiplier = "  + int(_levelEditPanel.btuMultiplier.value) + "\n";
			
			
			switch(_stageSelect){
				
				case 0:
					s += "stageState.stageType = StageTypes.HYDROLYSIS\n";
					s += "stageState.bacteriaType = BacteriaTypes.ARCHIE\n";
					break;
				case 1:
					s += "stageState.stageType = StageTypes.ACIDOGENESIS\n";
					s += "stageState.bacteriaType = BacteriaTypes.CLOSTER\n";
					break;
				case 2:
					s += "stageState.stageType = StageTypes.ACETOGENESIS\n";
					s += "stageState.bacteriaType = BacteriaTypes.BAXTER\n";
					break;
				case 3:
					s += "stageState.stageType = StageTypes.METHANOGENESIS\n";
					s += "stageState.bacteriaType = BacteriaTypes.SABBY\n";
					break;
			}

			s += "stageState.timeLimit = " + _levelEditPanel.timeLimit + "\n";
			s += "stageState.maxBacteriaForce = " + _levelEditPanel.bacteriaForce.value + "\n";  
			s += "stageState.maxBacteriaSpeed = " + _levelEditPanel.bacteriaSpeed.value + "\n";
			s += "stageState.currentHeat = " + _levelEditPanel.heatStart.value + "\n";
			s += "stageState.maxHeatOptimal  = " + _levelEditPanel.heatMaxMin.highValue + "\n";
			s += "stageState.minHeatOptimal = " + _levelEditPanel.heatMaxMin.lowValue + "\n";
			s += "stageState.coolRate = " + _levelEditPanel.heatCoolRate.value + "\n";
			s += "stageState.heatRate = " + _levelEditPanel.heatHeatRate.value + "\n";
			s += "stageState.rateOfReaction = " + int(_levelEditPanel.reaction.value) + "\n";
						
			if(_levelHolder.compounds.length > 0){
				
				s += "stageState.pooPostion.push(" 
				
				for (var i:int = 0; i < _levelHolder.compounds.length; i++) 
				{
					s += "new Point("+_levelHolder.compounds[i].x +" , "+ _levelHolder.compounds[i].y + ")";
					
					if(i != _levelHolder.compounds.length-1){
						s += ",\n";
					}
					
				}			
				
				s += ");\n\n"
					
			}
			
			if(_levelHolder.bacterias.length > 0){
				
				s += "stageState.bacteriaPostion.push(" 
					
				for (var j:int = 0; j < _levelHolder.bacterias.length; j++) 
				{
					s += "new Point("+_levelHolder.bacterias[j].x +" , "+ _levelHolder.bacterias[j].y + ")";
					
					if( j != _levelHolder.bacterias.length-1 ){
						s += ",\n";
					}
				}
				
				s += ");\n\n"
					
			}
			
			if(_levelHolder.stickies.length > 0){
				
				s += "stageState.stickyPosition.push(" 
				
				for (var k:int = 0; k < _levelHolder.stickies.length; k++) 
				{
					s += "{pos:new Point("+_levelHolder.stickies[k].x +" , "+ _levelHolder.stickies[k].y + "), scale:" + _levelHolder.stickies[k].size + "}";
					
					if( k != _levelHolder.stickies.length-1 ){
						s += ",\n";
					}
				}
				
				s += ");\n\n"
				
			}
			
			if(_levelHolder.killers.length > 0){
				
				s += "stageState.killerPosition.push(" 
				
				for (var l:int = 0; l < _levelHolder.killers.length; l++) 
				{
					s += "{pos:new Point("+_levelHolder.killers[l].x +" , "+ _levelHolder.killers[l].y + "), scale:" + _levelHolder.killers[l].size + "}";
					
					if( l != _levelHolder.killers.length-1 ){
						s += ",\n";
					}
				}
				
				s += ");\n\n"
				
			}
			
			_xml = <xml><code>{s}</code>
				<vars>
					<gemsPerCompound>{int(_levelEditPanel.compounds.value)}</gemsPerCompound>
					<totalGemsPossible>{_levelHolder.compounds.length * int(_levelEditPanel.compounds.value)}</totalGemsPossible>
					<btuMultiplier>{int(_levelEditPanel.btuMultiplier.value)}</btuMultiplier>
					<stageType>{_stageSelect}</stageType>
					<timeLimit>{_levelEditPanel.timeLimit}</timeLimit>
					<maxBacteriaForce>{_levelEditPanel.bacteriaForce.value}</maxBacteriaForce>
					<maxBacteriaSpeed>{_levelEditPanel.bacteriaSpeed.value}</maxBacteriaSpeed>
					<heatStart>{_levelEditPanel.heatStart.value}</heatStart>
					<maxHeatOptimal>{_levelEditPanel.heatMaxMin.highValue}</maxHeatOptimal>
					<minHeatOptimal>{_levelEditPanel.heatMaxMin.lowValue}</minHeatOptimal>
					<coolRate>{_levelEditPanel.heatCoolRate.value}</coolRate>
					<heatRate>{_levelEditPanel.heatHeatRate.value}</heatRate>
					<rateOfReaction>{int(_levelEditPanel.reaction.value)}</rateOfReaction>
					
					<bacteriaPosition />
					<pooPosition />
					<stickyPosition />
					<killerPosition />
			
				</vars>
			</xml>
			
			for (var i2:int = 0; i2 < _levelHolder.compounds.length; i2++) 
			{
				_xml.vars.pooPosition.appendChild(<item x={_levelHolder.compounds[i2].x} y={_levelHolder.compounds[i2].y} />);
			}
			
			for (var i3:int = 0; i3 < _levelHolder.bacterias.length; i3++) 
			{
				_xml.vars.bacteriaPosition.appendChild(<item x={_levelHolder.bacterias[i3].x} y={_levelHolder.bacterias[i3].y} />);
			}
			
			for (var i4:int = 0; i4 < _levelHolder.stickies.length; i4++) 
			{
				_xml.vars.stickyPosition.appendChild(<item x={_levelHolder.stickies[i4].x} y={_levelHolder.stickies[i4].y} size={_levelHolder.stickies[i4].size} />);
			}
			
			for (var i5:int = 0; i5 < _levelHolder.killers.length; i5++) 
			{
				_xml.vars.killerPosition.appendChild(<item x={_levelHolder.killers[i5].x} y={_levelHolder.killers[i5].y} size={_levelHolder.killers[i5].size} />);
			}
			
			trace(_xml);
			
			var ba:ByteArray = new ByteArray();
			ba.writeUTFBytes(_xml);
			
			var fr:FileReference = new FileReference();
			fr.addEventListener(Event.SELECT, _onRefSelect);
			fr.addEventListener(Event.CANCEL, _onRefCancel);
			
			fr.save(ba, _fileName);
		}
		
		private function _onRefSelect(e:Event):void
		{
			trace('select');
		}
		
		private function _onRefCancel(e:Event):void
		{
			trace('cancel');
		}
		
		private function levelLoad():void
		{
			_fileReference = new FileReference();
			var fileFilter:FileFilter=new FileFilter("BioTap","*.biotap");
			_fileReference.addEventListener(Event.SELECT, onSelectFile);
			_fileReference.addEventListener(Event.COMPLETE,onFileComplete);
			
			_fileReference.browse([fileFilter]);
	
		}
		
		private function onFileComplete(event:Event):void {
						
			_xml = new XML(_fileReference.data);
			
			_fileName = _fileReference.name;
			
			_levelEditPanel.changeStage(int(_xml.vars.stageType));
			_levelEditPanel.timeLimit = int(_xml.vars.timeLimit);
			
			_levelEditPanel.compounds.value = Number(_xml.vars.gemsPerCompound);
			_levelEditPanel.btuMultiplier.value = Number(_xml.vars.btuMultiplier);
			_levelEditPanel.bacteriaForce.value = Number(_xml.vars.maxBacteriaForce);
			_levelEditPanel.bacteriaSpeed.value = Number(_xml.vars.maxBacteriaSpeed);
			_levelEditPanel.heatStart.value = int(_xml.vars.heatStart);
			_levelEditPanel.heatMaxMin.highValue = Number(_xml.vars.maxHeatOptimal);
			_levelEditPanel.heatMaxMin.lowValue = Number(_xml.vars.minHeatOptimal);
			_levelEditPanel.heatCoolRate.value = Number(_xml.vars.coolRate);
			_levelEditPanel.heatHeatRate.value = Number(_xml.vars.heatRate);
			_levelEditPanel.reaction.value = Number(_xml.vars.rateOfReaction);
			
//			trace(_xml);
			
			var pooList:XMLList = _xml.vars.pooPosition;
			
			for (var i:int = 0; i < pooList.item.length(); i++) 
			{
				_levelHolder.addCompound(pooList.item[i].@x, pooList.item[i].@y);
			}
			
			var bacteriaList:XMLList = _xml.vars.bacteriaPosition;
			
			for (var j:int = 0; j < bacteriaList.item.length(); j++) 
			{
				_levelHolder.addBacteria(bacteriaList.item[j].@x, bacteriaList.item[j].@y);
			}
			
			var stickyList:XMLList = _xml.vars.stickyPosition;
			
			for (var k:int = 0; k < stickyList.item.length(); k++) 
			{
				_levelHolder.addSticky(stickyList.item[k].@x, stickyList.item[k].@y, stickyList.item[k].@size);
			}
			
			var killerList:XMLList = _xml.vars.killerPosition;
			
			for (var m:int = 0; m < killerList.item.length(); m++) 
			{
				_levelHolder.addKiller(killerList.item[m].@x, killerList.item[m].@y, killerList.item[m].@size);
			}
			
			_levelEditPanel.numCompounds = pooList.item.length();
			
		}
				
		private function onSelectFile(event:Event):void {
			_fileReference.load();
		}
		
		private function levelPlay():void
		{
			_isOpen = false;
			TweenLite.to(_levelEditPanel, 0.8, {x:-530, ease:Quint.easeInOut});
			
			_levelHolder.visible = false;
			
			//GAME PLAY CLASS			
			_gp = new GamePlayEditor();
			_gp.addEventListener(Event.INIT, contextLoaded);
			
			addChild(_gp);
		}		
		
		/********************************************************
		 * 
		 * 	Game State Handler 
		 * 
		 ********************************************************/
		protected function contextLoaded(event:Event):void
		{
						
			_gp.removeEventListener(Event.INIT, contextLoaded);
			
			stageState = new StageState();
			
			stageState.timeLimit = _levelEditPanel.timeLimit;
			
			
			//			/************************************ 
			//			 * Bad Bacteria vars 
			//			 ************************************/
			//			stageState.badBacteriaInfection = 0.6;
			//			stageState.badBacteriaType = BadGuyTypes.DOUCHEBAG;
			//			stageState.badBacteriaRadius = 20;
			//			stageState.maxBadBacteriaForce = 1;
			//			stageState.maxBadBacteriaSpeed = 1.8;
			
			for (var i:int = 0; i < _levelHolder.compounds.length; i++) 
			{
				stageState.pooPostion.push(new Point(_levelHolder.compounds[i].x, _levelHolder.compounds[i].y));
			}
			
			for (var j:int = 0; j < _levelHolder.bacterias.length; j++) 
			{
				stageState.bacteriaPostion.push(new Point(_levelHolder.bacterias[j].x, _levelHolder.bacterias[j].y));
			}
			
			for (var k:int = 0; k < _levelHolder.stickies.length; k++) 
			{
				stageState.stickyPosition.push({pos:new Point(_levelHolder.stickies[k].x, _levelHolder.stickies[k].y), scale:_levelHolder.stickies[k].size});
			}
			
			for (var l:int = 0; l < _levelHolder.killers.length; l++) 
			{
				stageState.killerPosition.push({pos:new Point(_levelHolder.killers[l].x, _levelHolder.killers[l].y), scale:_levelHolder.killers[l].size});
			}
			
			/** Bacteria variables */
			stageState.maxBacteriaForce = _levelEditPanel.bacteriaForce.value;
			stageState.maxBacteriaSpeed = _levelEditPanel.bacteriaSpeed.value;
			
			/** Heat Control variables */
			stageState.currentHeat = _levelEditPanel.heatStart.value;
			stageState.maxHeatOptimal  = _levelEditPanel.heatMaxMin.highValue;
			stageState.minHeatOptimal = _levelEditPanel.heatMaxMin.lowValue;
			
			stageState.coolRate = _levelEditPanel.heatCoolRate.value;
			stageState.heatRate = _levelEditPanel.heatHeatRate.value;
			
			/** Poo variables */
			
			stageState.rateOfReaction = int(_levelEditPanel.reaction.value);
			
			//stage 0 1 2 3
			switch(_stageSelect){
				case 0:
					stageState.stageType = StageTypes.HYDROLYSIS;
					stageState.bacteriaType = BacteriaTypes.ARCHIE;
					break;
				case 1:
					stageState.stageType = StageTypes.ACIDOGENESIS;
					stageState.bacteriaType = BacteriaTypes.CLOSTER;
					break;
				case 2:
					stageState.stageType = StageTypes.ACETOGENESIS;
					stageState.bacteriaType = BacteriaTypes.BAXTER;
					break;
				case 3:
					stageState.stageType = StageTypes.METHANOGENESIS;
					stageState.bacteriaType = BacteriaTypes.SABBY;
					break;
					
			}
			
			_gp.startLevel(_stageSelect, _levelHolder.compounds.length, int(_levelEditPanel.compounds.value), int(_levelEditPanel.btuMultiplier.value), stageState);
			_gp.levelFinished.add(editLevel);
			
		}
		
		private function editLevel():void
		{
			_levelHolder.visible = true;
			_isOpen = false;
			TweenLite.to(_levelEditPanel, 0.8, {x:0, ease:Quint.easeInOut});
		}
		
		/**********************************************************************
		 * 
		 * 	Clean Up
		 * 
		 **********************************************************************/
		private function cleanGame():void
		{
			_gp.hide();
		}
				
	}
}