package items
{
	import flash.events.MouseEvent;

	public class BacteriaBaxter extends baxterMC
	{
		
		public function BacteriaBaxter()
		{
			super();
			
			this.rotation = Math.random() * 360;
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, startD);
		}
		
		protected function startD(event:MouseEvent):void
		{
			this.stage.addEventListener(MouseEvent.MOUSE_UP, stopD);
			this.startDrag(true);
		}
		
		protected function stopD(event:MouseEvent):void
		{
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, stopD);
			this.stopDrag();
		}
	}
}