package items
{
	import flash.events.MouseEvent;

	public class Compound extends compoundMC
	{
		public function Compound()
		{
			super();
			
			this.scaleX = 0.5;
			this.scaleY = 0.5;
			this.alpha = 0.5;
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, startD);
			
		}
		
		protected function startD(event:MouseEvent):void
		{
			this.stage.addEventListener(MouseEvent.MOUSE_UP, stopD);
			this.startDrag(true);
		}
		
		protected function stopD(event:MouseEvent):void
		{
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, stopD);
			this.stopDrag();
		}
		
		
	}
}