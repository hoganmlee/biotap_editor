package items
{
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	public class BacteriaSticky extends stickyMC
	{
		
		public function BacteriaSticky()
		{
			super();
			
			this.mc.addEventListener(MouseEvent.MOUSE_DOWN, startD);
			this.resizeMC.addEventListener(MouseEvent.MOUSE_DOWN, startResize);
			
			resizeSticky();
		}
		
		public function get size():Number
		{
			return this.mc.scaleX;
		}
		
		public function set size(value:Number):void
		{
			this.mc.scaleX = this.mc.scaleY = value;
			this.resizeMC.x = this.barMC.width * value;
		}
		
		protected function startResize(event:MouseEvent):void
		{
			this.resizeMC.removeEventListener(MouseEvent.MOUSE_DOWN, startResize);
			this.resizeMC.addEventListener(MouseEvent.MOUSE_UP, stopResize);
			this.resizeMC.addEventListener(MouseEvent.MOUSE_MOVE, resizeSticky);
			this.resizeMC.startDrag(true, new Rectangle(this.barMC.x, this.barMC.y, this.barMC.width, 0));
		}
		
		protected function stopResize(event:MouseEvent):void
		{
			this.resizeMC.addEventListener(MouseEvent.MOUSE_DOWN, startResize);
			this.resizeMC.removeEventListener(MouseEvent.MOUSE_UP, stopResize)
			this.resizeMC.removeEventListener(MouseEvent.MOUSE_MOVE, resizeSticky);
			this.resizeMC.stopDrag();
		}
		
		private function resizeSticky(e:MouseEvent=null):void
		{
			var scale:Number = this.resizeMC.x / this.barMC.width;
			
			if(scale<= 0){
				
				this.mc.scaleX = 0.1;
				this.mc.scaleY = 0.1;
				
			}else{
			
				this.mc.scaleX = scale;
				this.mc.scaleY = scale;	
				
			}
		}
		
		protected function startD(event:MouseEvent):void
		{
			this.stage.addEventListener(MouseEvent.MOUSE_UP, stopD);
			this.startDrag();
		}
		
		protected function stopD(event:MouseEvent):void
		{
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, stopD);
			this.stopDrag();
		}
		
		
	}
}