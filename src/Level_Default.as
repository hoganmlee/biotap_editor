package
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.game.base.DefaultGame;
	import com.sajakfarki.biopets.game.base.ILevel;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.game.scenes.panels.Level11_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.StageCompletePanel;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.*;
	
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import org.osflash.signals.Signal;
	
	public class Level_Default extends DefaultGame implements ILevel
	{
		private var _stageVars:StageState;
		
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var stageState : StageState;
		
		private var nextStageSignal:NextStage = new NextStage();
		
		/** Background texture */
		[Embed(source = "../../biotap/media/backgrounds/level1stage1.jpg")]
		private var _bgTexture:Class;
		
		[Embed(source = "../../biotap/media/titles/stage_level_panel.png")]
		private var _titleTexture:Class;
		
		[Embed(source = "../../biotap/media/titles/level1.png")]
		private var _levelTexture:Class;
		
		[Embed(source = "../../biotap/media/titles/stage1.png")]
		private var _stageTexture:Class;		
		
		private var _infoPanel:Level11_Panel;
		private var _stageComplete:StageCompletePanel;
		
		
		public function Level_Default(contextView:Scene2D, gb:GameBatches, w:Number, h:Number, gemsPerCompound:int, stageVars:StageState)
		{
			_stageVars = stageVars;
			
			var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			var bgS:Sprite2D = new Sprite2D(bgT);
			
			var tT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _titleTexture()).bitmapData);
			var titleSprite:Sprite2D = new Sprite2D(tT);
			
			var lT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _levelTexture()).bitmapData);
			var lS:Sprite2D = new Sprite2D(lT);
			
			var sT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _stageTexture()).bitmapData);
			var sS:Sprite2D = new Sprite2D(sT);
			
			titleSprite.addChild(lS);
			titleSprite.addChild(sS);
			
			/** Show Stage Screen */
			_stageComplete = new StageCompletePanel();
			_stageComplete.x = w/2;
			_stageComplete.y = h/2;
			_stageComplete.visible = false;
			_stageComplete.alpha = 0;
			_stageComplete.scaleX = 0.2;
			_stageComplete.scaleY = 0.2;
			
			contextView.addChildAt(_stageComplete, contextView.numChildren);
			
			_infoPanel = new Level11_Panel();
			
			super(contextView, 
					gb,
					w, 
					h,
					bgS,
					titleSprite,
					_infoPanel,
					gemsPerCompound);
			
			stageS = stageState;
			
			init();
			
		}
			
		/**************************************************
		 * 
		 * 	Show Level Complete
		 * 
		 **************************************************/	
		public function showLevelComplete(tweenTime:Number, delay:Number):void{
			
		}
		
		public function showStageComplete(tweenTime:Number, delay:Number):void
		{
			_stageComplete.visible = true;
			TweenMax.to(_stageComplete, tweenTime, {delay:delay, scaleX:1, scaleY:1, alpha:1, ease:Back.easeOut});
			
			_stageComplete.mouseEnabled = true;
			_stageComplete.addEventListener(MouseEvent.CLICK, gotoNextStage);
		}
		
		private function gotoNextStage(event:MouseEvent):void
		{
			_stageComplete.removeEventListener(MouseEvent.CLICK, gotoNextStage);
			nextStageSignal.dispatch();
		}		
		
		public function get initialized():LevelInitialized
		{
			return stageState.levelInitialized;
		}
		
		public function get nextStage():NextStage
		{
			return nextStageSignal;
		}
		
		public function get stageFinished():StageFinished
		{
			return stageState.stageFinished;
		}
				
		override protected function startUp() : void
		{
			
			stageState.timeLimit = _stageVars.timeLimit;
			
			stageState.stageType = _stageVars.stageType
			
			stageState.bacteriaType = _stageVars.bacteriaType;
			
			stageState.bacteriaPostion = _stageVars.bacteriaPostion;
			
			stageState.pooPostion = _stageVars.pooPostion;
			
			stageState.stickyPosition = _stageVars.stickyPosition
			
			stageState.killerPosition = _stageVars.killerPosition;
			
			stageState.maxBacteriaForce = _stageVars.maxBacteriaForce;
			stageState.maxBacteriaSpeed = _stageVars.maxBacteriaSpeed;
			
			stageState.currentHeat = _stageVars.currentHeat;
			
			stageState.maxHeatOptimal  = _stageVars.maxHeatOptimal;
			stageState.minHeatOptimal = _stageVars.minHeatOptimal;
			
			stageState.coolRate = _stageVars.coolRate;
			stageState.heatRate = _stageVars.heatRate;
			
			stageState.rateOfReaction = _stageVars.rateOfReaction;
			
			//stageState.hud.add(dispatchHUD);
			//stageState = _stageVars;
			
			/** Stage variables */
			stageState.width = contextView.stage.stageWidth;
			stageState.height = contextView.stage.stageHeight;
			
			stageState.init = false;
			stageState.paused = true;
			stageState.finished = false;
			stageState.showHeat = true;
			stageState.tapRadius = 100;
			
			//			/************************************ 
			//			 * Bad Bacteria vars 
			//			 ************************************/
			//			stageState.badBacteriaInfection = 0.6;
			//			stageState.badBacteriaType = BadGuyTypes.DOUCHEBAG;
			//			stageState.badBacteriaRadius = 20;
			//			stageState.maxBadBacteriaForce = 1;
			//			stageState.maxBadBacteriaSpeed = 1.8;
			
			/** Bacteria variables */
			stageState.bacteriaRadius = 10;
			
			/** Heat Control variables */
			stageState.maxHeatTemp = 100;
			stageState.minHeatTemp = 0;
			
			stageState.heatStep = 0.2;
			
			/** Poo variables */
			stageState.compoundType = CompoundTypes.POO;
			stageState.minBacteria = 0;
			stageState.maxBacteria = 15;
			
			stageState.minPooRadius = 119;
			stageState.maxPooRadius = 120;
			
			stageState.rateOfRadiusExpand = 0.8;
			stageState.rateOfRadiusCollapse = 0.8;
			
			stageState.rateOfPop = 30;
			stageState.popTime = 30;
			stageState.popCount = 0;
			
			stageState.reactionStartTime = 100;
			
			super.startUp();
			
			
			/** Add Systems */
			addSystems();
			
		}
		
		override protected function mapInjectors() : void
		{
			super.mapInjectors();
			
			injector.mapSingleton( LevelCreator );
			injector.mapSingleton( StageState );
			injector.mapSingleton( PreUpdate );
			injector.mapSingleton( Update );
			injector.mapSingleton( Move );
			injector.mapSingleton( ResolveCollisions );
			injector.mapSingleton( Render );
			
			//injector.mapValue( KeyPoll, new KeyPoll( contextView.stage ) );
			
			injector.injectInto( this );
			
		}
		
		public function addSystems() : void
		{
			systemManager.addSystem( GameManager );
			
			systemManager.addSystem( TapSystem );
			systemManager.addSystem( HeatControlSystem );
			systemManager.addSystem( PooSystem );
			
			systemManager.addSystem( OrbitSystem );
			systemManager.addSystem( MotionSystem );
			
			systemManager.addSystem( CollisionSystem );
			systemManager.addSystem( RenderSystem );
			systemManager.addSystem( ProcessManager );			
		}
		
		public function finish():void
		{
			systemManager.removeSystem(PooSystem);
			systemManager.removeSystem(OrbitSystem);
			systemManager.removeSystem(HeatControlSystem);
		}
		
		public function disposeLevel():void
		{
			dispose();
		}
	}
}