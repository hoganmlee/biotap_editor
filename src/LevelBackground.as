package
{
	import items.BacteriaBaxter;
	import items.BacteriaKiller;
	import items.BacteriaSticky;
	import items.Compound;

	public class LevelBackground extends LevelStage
	{
		
		private var _compounds:Array;
		private var _bacterias:Array;
		private var _stickies:Array;
		private var _killers:Array;
		
		private var _width:int;
		private var _height:int;
		
		
		public function LevelBackground(width:int, height:int)
		{
			super();
			
			_height = height;
			_width = width;
			
			_compounds = new Array();
			_bacterias = new Array();
			_stickies = new Array();
			_killers = new Array();
			
		}
		
		public function get killers():Array
		{
			return _killers;
		}
		
		public function get bacterias():Array
		{
			return _bacterias;
		}

		public function get stickies():Array
		{
			return _stickies;
		}

		public function get compounds():Array
		{
			return _compounds;
		}

		public function addCompound(px:int=-1, py:int=-1):void
		{
			var c:Compound = new Compound();
			
			if(px == -1 && py==-1){
				c.x = _width/2;
				c.y = _height/2;
			}else{
				c.x = px;
				c.y = py;
			}
			
			addChild(c);
			
			_compounds.push(c);			
		}
		
		public function addBacteria(px:int=-1, py:int=-1):void
		{
			var b:BacteriaBaxter = new BacteriaBaxter();
			
			if(px == -1 && py==-1){
				b.x = _width/2;
				b.y = _height/2;
			}else{
				b.x = px;
				b.y = py;
			}
			
			addChild(b);
			
			_bacterias.push(b);
		}
		
		public function addKiller(px:int=-1, py:int=-1, size:Number=-1):void
		{
			var k:BacteriaKiller = new BacteriaKiller();
			
			if(px == -1 && py==-1 && size==-1){
				k.x = _width/2;
				k.y = _height/2;
			}else{
				k.x = px;
				k.y = py;
				k.size = size;
			}
			
			addChild(k);
				
			_killers.push(k);
		}
		
		public function addSticky(px:int=-1, py:int=-1, size:Number=-1):void
		{
			var s:BacteriaSticky = new BacteriaSticky();
			
			if(px == -1 && py==-1 && size==-1){
				s.x = _width/2;
				s.y = _height/2;
			}else{
				s.x = px;
				s.y = py;
				s.size = size;
			}
						
			addChild(s);
			
			_stickies.push(s);
		}
		
		public function removeCompound():void
		{
			if(_compounds.length > 0) 
			{
				removeChild(_compounds[_compounds.length-1]);
				_compounds.pop();
			}
		}
		
		public function removeBacteria():void
		{
			if(_bacterias.length > 0) 
			{
				removeChild(_bacterias[_bacterias.length-1]);
				_bacterias.pop();
			}
		}
		
		public function removeKiller():void
		{
			if(_killers.length > 0) 
			{
				removeChild(_killers[_killers.length-1]);
				_killers.pop();
			}
		}
		
		public function removeSticky():void
		{
			if(_stickies.length > 0) 
			{
				removeChild(_stickies[_stickies.length-1]);
				_stickies.pop();
			}
		}
			
		
	}
}